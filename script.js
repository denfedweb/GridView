const dataExample = [
    {
        company: "Alfreds <b>Futterkise</b>",
        chef: "Maria Anders",
        country: "Germany"
    },
    {
        company: "Centro comercial Moctezuma",
        chef: "Francesco Chang",
        country: "Mexico"
    },
    {
        company: "Ernst Handel",
        chef: "Roland Mendel",
        country: "Austria"
    },
    {
        company: "Island Trading",
        chef: "Helen Bennett",
        country: "UK"
    },
    {
        company: "Laughing Bacchus Winecellars",
        chef: "Yoshi Tannamuri",
        country: "Canada"
    }
];