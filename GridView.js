class GridView {
    /**
     * properties 
     * @param [array] _tableClass - ксс классы оформления
     * @param [array] data - данные вывода
     * @param [array] _attribute - управление выводом данных
     * @param [array] _element - место вывода таблицы
     * @param [array] _header - заголовок таблицы
     * @param [array] _headerClass - ксс классы заголовка
    */

    constructor() {
        this._header = '';
        this._headerClass = [];
        this._element = 'body';
        this._tableClass = [];
        this._attribute = [];
    }

    /**
     * Method set header
    */

     set header(header) {
         if (typeof header === "string" && header.trim() != ''){
             this._header = header.trim();
             return true
         }

         return false
     }

    /**
     * Method set headerClass
    */

    set headerClass(headerClass) {
        if (typeof headerClass === "object"){
            this._headerClass = headerClass;
            return true
        }

        return false
    }

    /**
     * Method set element
    */

    set element(element) {
        if (elemExist = document.querySelector(element)) {
            this._element = elemExist;

            return true;
        } 

        return false
    }   

    /**
     * Method for show GridViewTable
    */

     render() {

     }
}